import java.io.*;
import java.nio.ByteBuffer;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;

public class Testing {
    // constants for linear pi operation
    private static final long MB_0 = 0xCFFCCFFCL;
    private static final long MB_1 = 0xF33FF33FL;
    private static final long MB_2 = 0xFCCFFCCFL;
    private static final long MB_3 = 0x3FF33FF3L;

    // constants for key widening
    private static final long MC0 = 0xACACACACL;
    private static final long MC1 = 0x59595959L;
    private static final long MC2 = 0xB2B2B2B2L;
    private static final long MC3 = 0x65656565L;

    // flag for tabs are generated or not
    private static boolean tab_gen = false;

    // permutation tables
    private static int[] p0 = {15, 14, 10, 1, 11, 5, 8, 13, 9, 3, 2, 7, 0, 6, 4, 12};
    private static int[] p1 = {11, 10, 13, 7, 8, 14, 0, 5, 15, 6, 3, 4, 1, 9, 2, 12};
    private static int[] ip0 = {12, 3, 10, 9, 14, 5, 13, 11, 6, 8, 2, 4, 15, 7, 1, 0};
    private static int[] ip1 = {6, 12, 14, 10, 11, 7, 9, 3, 4, 13, 1, 0, 15, 2, 5, 8};

    // tables
    private static int[][] s_box = new int[4][256];
    private static long[][] s_tab = new long[4][256];
    private static long[] ce = new long[52]; // cipher text encrypted
    private static long[] cd = new long[52]; // cipher text decrypted
    private static long[] e_key = new long[52]; // encryption key
    private static long[] d_key = new long[52]; // decryption key
    private static long[] in_b = new long[4]; // input block = 4 * 8 = 32 byte = 256 bit

    /**
     * Function used in key widening operation (to shift the key's left part)
     * @param x is number to be shifted
     * @param n argument for shifting
     * @return shifted number
     */
    private static long rotl(long x, int n) {
        // << cycle shifting
        // <<< shifting with zero filling
        return ((x << n) | (x >>> (32 - n))) & 0xFFFFFFFFL;
    }

    /**
     * Function used in key widening operation (to shift the key's right part)
     * @param x is number to be shifted
     * @param n argument for shifting
     * @return shifted number
     */
    private static long rotr(long x, int n) {
        // lst bits
        return ((x << (32 - n)) | (x >>> n)) & 0xFFFFFFFFL;
    }

    /**
     * Function used to take even bits using a mask
     * @param n a number from which it's needed to take even bits
     */
    private static long msk(int n) {
        return ((0x000000FFL >>> n) * 0x01010101L);
    }

    /**
     * Function is used to shift byte during the key widening operations. Here mask used to take even bits.
     * @param x a number to be shifted
     * @param n argument of shifting
     * @return shifted result
     */
    private static long brotl(long x, int n) {
//        return ((x & msk(n)) << n) | ((x & (~msk(n) & 0xFFFFFFFFL)) >>> (8 - n));
        return (((x & msk(n)) << n) | ((x & (~msk(n) & 0xFFFFFFFFL)) >>> (8 - n))) & 0xFFFFFFFFL;
    }

    /**
     * Function returns the value of needed byte
     * @param x a number from which to take a byte
     * @param n the number of needed byte
     * @return value of needed byte
     */
    private static int __byte(long x, int n) {
        return (int) ((x >>> (8 * n)) & 0xFF);
    }

    /**
     * Functiom used for counting round keys from widened
     * @param x number before mutation
     * @return mutated number
     */
    private static long row_perm(long x) {
//        return (x & MB_0) ^ (rotl(x, 8) & MB_1) ^ (rotl(x, 16) & MB_2) ^ (rotl(x, 24) & MB_3);
        return ((x & MB_0) ^ (rotl(x, 8) & MB_1) ^ (rotl(x, 16) & MB_2) ^ (rotl(x, 24) & MB_3)) & 0xFFFFFFFFL;
    }

    /**
     * gamma and pi for EVEN rounds
     * @param y basic block
     * @param x mutated block
     * @param i number or S-tab
     * @param k element in S-tab
     */
    private static void fr0(long[] y, long[] x, int i, long k) { // 0'th function of round
        y[i] =   (s_tab[ i         ][__byte(x[0], i)]
                ^ s_tab[(i + 1) & 3][__byte(x[1], i)]
                ^ s_tab[(i + 2) & 3][__byte(x[2], i)]
                ^ s_tab[(i + 3) & 3][__byte(x[3], i)] ^ k) & 0xFFFFFFFFL;
    }

    /**
     * gamma and pi for ODD rounds
     * @param y    basic block
     * @param x    mutated block
     * @param i    number or S-tab
     * @param k    element in S-tab
     * @param idx  **
     */
    private static void fr1(long[] y, long[] x, int i, long k, int idx) { // 1'st function of round
        y[i + idx] = (s_tab[(i + 2) & 3][__byte(x[0], i)]
                    ^ s_tab[(i + 3) & 3][__byte(x[1], i)]
                    ^ s_tab[i          ][__byte(x[2], i)]
                    ^ s_tab[(i + 1) & 3][__byte(x[3], i)] ^ k) & 0xFFFFFFFFL;
    }

    /**
     * Function of EVEN round for whole block
     * @param kp  key
     * @param b0  block of information
     * @param b1  mutated
     * @param idx **
     */
    private static void f0_rnd(long[] kp, long[] b0, long[] b1, int idx) {
        for (int i = 0; i < 4; i++) {
            fr0(b1, b0, i, kp[i + idx]);
        }
    }

    /**
     * Same function for ODD rounds
     */
    private static void f1_rnd(long[] kp, long[] b0, long[] b1, int idx) {
        for (int i = 0; i < 4; i++) {
            fr1(b0, b1, i, kp[i + idx], 0);
        }
    }

    /**
     * Function is using genrated tables to produce TAU operation
     * @param i  index of S-tab
     * @param b0 input data
     * @param b1 out data
     */
    private static void gamma_tau(int i, long[] b0, long[] b1) {
        b0[i] =   (s_box[(i + 2) & 3][__byte(b1[0], i)]
                | (s_box[(i + 3) & 3][__byte(b1[1], i)] << 8)
                | (s_box[ i         ][__byte(b1[2], i)] << 16)
                | (s_box[(i + 1) & 3][__byte(b1[3], i)] << 24)) & 0xFFFFFFFFL;
    }

    /**
     * Function takes care of bit permutation amd generates S-tabs
     */
    private static void gen_tab() {
        int i;
        long xl, xr, y, yl, yr;//, t;
        // bit permutations
        for (i = 0; i < 256; i++) {
            xl = p1[i >>> 4];
            xr = p0[i & 15];

            yl = (xl & 0x0e) ^ ((xl << 3) & 0x08) ^ ((xl >>> 3) & 0x01) ^ ((xr << 1) & 0x0a) ^ ((xr << 2) & 0x04) ^ ((xr >>> 2) & 0x02) ^ ((xr >>> 1) & 0x01);

            yr = (xr & 0x0d) ^ ((xr << 1) & 0x04) ^ ((xr >>> 1) & 0x02) ^ ((xl >>> 1) & 0x05) ^ ((xl << 2) & 0x08) ^ ((xl << 1) & 0x02) ^ ((xl >>> 2) & 0x01);

            y = (ip0[(int) yl] | (ip1[(int) yr] << 4));

            yr = (((y << 3) | (y >>> 5)) & 255);
            xr = (((i << 3) | (i >>> 5)) & 255);
            yl = (((y << 1) | (y >>> 7)) & 255);
            xl = (((i << 1) | (i >>> 7)) & 255);

            //
            s_box[0][i] = (int) yl;
            s_box[1][i] = (int) yr;
            s_box[2][(int) xl] = (int) y;
            s_box[3][(int) xr] = (int) y;

            s_tab[0][i] = (yl * 0x01010101L) & 0x3fcff3fcL;
            s_tab[1][i] = (yr * 0x01010101L) & 0xfc3fcff3L;
            s_tab[2][(int) xl] = (y * 0x01010101L) & 0xf3fc3fcfL;
            s_tab[3][(int) xr] = (y * 0x01010101L) & 0xcff3fc3fL;

        }

        //
        xl = 0xa54ff53aL & 0xFFFFFFFFL;

        // generating round keys
        for (i = 0; i < 13; i++) {
            ce[4 * i] = xl ^ MC0;
            ce[4 * i + 1] = xl ^ MC1;
            ce[4 * i + 2] = xl ^ MC2;
            ce[4 * i + 3] = xl ^ MC3;

            yl = row_perm((i & 1) > 0 ? xl : rotr(xl, 16));

            cd[4 * (12 - i)] = yl ^ MC0;
            cd[4 * (12 - i) + 1] = rotl(yl, 24) ^ MC1;
            cd[4 * (12 - i) + 2] = rotl(yl, 16) ^ MC2;
            cd[4 * (12 - i) + 3] = rotl(yl, 8) ^ MC3;

            xl += 0x3c6ef372L;
        }
    }

    /**
     * This function is used to widen the keys for initial theta operation, initial key addition and for
     * getting round keys
     */
    private static void setup(long[] in_key) {
        int i, j;
        long t0, t1;
        long[] tu = new long[4]; // 2k bytes of key
        long[] tv = new long[4]; // 2k+1 bytes of key
        long[] ek = new long[8];
        long[] dk = new long[8];

        if (!tab_gen) {
            gen_tab(); // generated tabs
            tab_gen = true;
        }

        // initial
        tu[0] = 0;
        tv[0] = 0;
        tu[1] = 0;
        tv[1] = 0;
        tu[2] = 0;
        tv[2] = 0;
        tu[3] = 0;
        tv[3] = 0;

        // to concatenate parts of keys, using shifting
        tu[3] = (__byte(in_key[6], 0) | (__byte(in_key[6], 2) << 8) | (__byte(in_key[7], 0) << 16) | (__byte(in_key[7], 2) << 24)) & 0xFFFFFFFFL;
        tv[3] = (__byte(in_key[6], 1) | (__byte(in_key[6], 3) << 8) | (__byte(in_key[7], 1) << 16) | (__byte(in_key[7], 3) << 24)) & 0xFFFFFFFFL;

        tu[2] = (__byte(in_key[4], 0) | (__byte(in_key[4], 2) << 8) | (__byte(in_key[5], 0) << 16) | (__byte(in_key[5], 2) << 24)) & 0xFFFFFFFFL;
        tv[2] = (__byte(in_key[4], 1) | (__byte(in_key[4], 3) << 8) | (__byte(in_key[5], 1) << 16) | (__byte(in_key[5], 3) << 24)) & 0xFFFFFFFFL;

        tu[0] = (__byte(in_key[0], 0) | (__byte(in_key[0], 2) << 8) | (__byte(in_key[1], 0) << 16) | (__byte(in_key[1], 2) << 24)) & 0xFFFFFFFFL;
        tv[0] = (__byte(in_key[0], 1) | (__byte(in_key[0], 3) << 8) | (__byte(in_key[1], 1) << 16) | (__byte(in_key[1], 3) << 24)) & 0xFFFFFFFFL;

        tu[1] = (__byte(in_key[2], 0) | (__byte(in_key[2], 2) << 8) | (__byte(in_key[3], 0) << 16) | (__byte(in_key[3], 2) << 24)) & 0xFFFFFFFFL;
        tv[1] = (__byte(in_key[2], 1) | (__byte(in_key[2], 3) << 8) | (__byte(in_key[3], 1) << 16) | (__byte(in_key[3], 3) << 24)) & 0xFFFFFFFFL;

        // one round of ciphering for both sequences
        fr0(ek, tu, 0, 0);
        fr0(ek, tu, 1, 0);
        fr0(ek, tu, 2, 0);
        fr0(ek, tu, 3, 0);
        fr1(ek, tv, 0, 0, 4);
        fr1(ek, tv, 1, 0, 4);
        fr1(ek, tv, 2, 0, 4);
        fr1(ek, tv, 3, 0, 4);

        // T0 and T1 to widen keys
        t0 = (ek[0] ^ ek[1] ^ ek[2] ^ ek[3]) & 0xFFFFFFFFL;
        t1 = (ek[4] ^ ek[5] ^ ek[6] ^ ek[7]) & 0xFFFFFFFFL;

        // 8 widen keys, using T0 and T1
        ek[0] ^= t1;
        ek[1] ^= t1;
        ek[2] ^= t1;
        ek[3] ^= t1;
        ek[4] ^= t0;
        ek[5] ^= t0;
        ek[6] ^= t0;
        ek[7] ^= t0;

        // now counting keys
        // FFFFFFFF is M(Ci-1) shifted left for 1 bit
        // these are for theta operation and first initial round
        d_key[48] = (ek[0] ^ ce[0]) & 0xFFFFFFFFL;
        d_key[49] = (ek[1] ^ ce[1]) & 0xFFFFFFFFL;
        d_key[50] = (ek[2] ^ ce[2]) & 0xFFFFFFFFL;
        d_key[51] = (ek[3] ^ ce[3]) & 0xFFFFFFFFL;

        // for ODD and EVEN widening keys:
        dk[0] = brotl(row_perm(rotr(ek[2], 16)), 4); // get byte shifting from the whole key shifted
        dk[1] = brotl(row_perm(rotr(ek[3], 24)), 2); // ==
        dk[2] = row_perm(rotr(ek[0], 24)); // ==
        dk[3] = brotl(row_perm(ek[1]), 2); // ==

        dk[4] = brotl(row_perm(ek[7]), 6);
        dk[5] = brotl(row_perm(rotr(ek[4], 24)), 6);
        dk[6] = brotl(row_perm(ek[5]), 4);
        dk[7] = brotl(row_perm(rotr(ek[6], 16)), 4);

        //
        for (i = 0, j = 0; i < 13; i++, j += 4) {
            if ((i & 1) > 0) { // if ODD
                e_key[j] = (ek[4] ^ ce[j]) & 0xFFFFFFFFL;
                e_key[j + 1] = (ek[5] ^ ce[j + 1]) & 0xFFFFFFFFL;
                e_key[j + 2] = (ek[6] ^ ce[j + 2]) & 0xFFFFFFFFL;
                e_key[j + 3] = (ek[7] ^ ce[j + 3]) & 0xFFFFFFFFL;

                t1 = ek[7];
                ek[7] = rotl(ek[6], 16);
                ek[6] = rotl(ek[5], 8);
                ek[5] = brotl(ek[4], 2);
                ek[4] = brotl(t1, 2);
            } else { // if EVEN
                e_key[j] = (ek[0] ^ ce[j]) & 0xFFFFFFFFL;
                e_key[j + 1] = (ek[1] ^ ce[j + 1]) & 0xFFFFFFFFL;
                e_key[j + 2] = (ek[2] ^ ce[j + 2]) & 0xFFFFFFFFL;
                e_key[j + 3] = (ek[3] ^ ce[j + 3]) & 0xFFFFFFFFL;

                t1 = ek[0];
                ek[0] = rotl(ek[1], 24);
                ek[1] = rotl(ek[2], 16);
                ek[2] = brotl(ek[3], 6);
                ek[3] = brotl(t1, 6);
            }
        }
        // D-keys all the same
        for (i = 0, j = 0; i < 12; i++, j += 4) {
            if ((i & 1) > 0) {
                d_key[j] =     (dk[4] ^ cd[j]);
                d_key[j + 1] = (dk[5] ^ cd[j + 1]);
                d_key[j + 2] = (dk[6] ^ cd[j + 2]);
                d_key[j + 3] = (dk[7] ^ cd[j + 3]);

                t1 = dk[5];
                dk[5] = rotl(dk[6], 16);
                dk[6] = rotl(dk[7], 24);
                dk[7] = brotl(dk[4], 6);
                dk[4] = brotl(t1, 6);
            } else {
                d_key[j] =     (dk[0] ^ cd[j]);
                d_key[j + 1] = (dk[1] ^ cd[j + 1]);
                d_key[j + 2] = (dk[2] ^ cd[j + 2]);
                d_key[j + 3] = (dk[3] ^ cd[j + 3]);

                t1 = dk[2];
                dk[2] = rotl(dk[1], 8);
                dk[1] = rotl(dk[0], 16);
                dk[0] = brotl(dk[3], 2);
                dk[3] = brotl(t1, 2);
            }
        }

        // the same initial keys but for EVEN rounds
        e_key[48] = row_perm(rotr(e_key[48], 16));
        e_key[49] = row_perm(rotr(e_key[49], 8));
        e_key[50] = row_perm(e_key[50]);
        e_key[51] = row_perm(rotr(e_key[51], 24));
    }

    /**
     * Encryption
     * @param in_blk input block
     */
    public static void encrypt(long[] in_blk) {
        long[] b0 = new long[4];
        long[] b1 = new long[4];

        System.arraycopy(in_blk, 0, in_b, 0, 4);

        // initial key xor'ing
        b0[0] = (in_blk[0] ^ e_key[0]);
        b0[1] = (in_blk[1] ^ e_key[1]);
        b0[2] = (in_blk[2] ^ e_key[2]);
        b0[3] = (in_blk[3] ^ e_key[3]);

        // 12 rounds using found keys
        f0_rnd(e_key, b0, b1, 4);
        f1_rnd(e_key, b0, b1, 8);
        f0_rnd(e_key, b0, b1, 12);
        f1_rnd(e_key, b0, b1, 16);
        f0_rnd(e_key, b0, b1, 20);
        f1_rnd(e_key, b0, b1, 24);
        f0_rnd(e_key, b0, b1, 28);
        f1_rnd(e_key, b0, b1, 32);
        f0_rnd(e_key, b0, b1, 36);
        f1_rnd(e_key, b0, b1, 40);
        f0_rnd(e_key, b0, b1, 44);

        // tau operation using s-boxes
        gamma_tau(0, b0, b1);
        gamma_tau(1, b0, b1);
        gamma_tau(2, b0, b1);
        gamma_tau(3, b0, b1);

        // last key xor'ing
        in_blk[0] = (b0[0] ^ e_key[48]);
        in_blk[1] = (b0[1] ^ e_key[49]);
        in_blk[2] = (b0[2] ^ e_key[50]);
        in_blk[3] = (b0[3] ^ e_key[51]);
    }

    /**
     * Decryption
     * @param in_blk input block
     */
    public static void decrypt(long[] in_blk) {
        long[] b0 = new long[4];
        long[] b1 = new long[4];

        b0[0] = (in_blk[0] ^ d_key[0]);
        b0[1] = (in_blk[1] ^ d_key[1]);
        b0[2] = (in_blk[2] ^ d_key[2]);
        b0[3] = (in_blk[3] ^ d_key[3]);

        f0_rnd(d_key, b0, b1, 4);
        f1_rnd(d_key, b0, b1, 8);
        f0_rnd(d_key, b0, b1, 12);
        f1_rnd(d_key, b0, b1, 16);
        f0_rnd(d_key, b0, b1, 20);
        f1_rnd(d_key, b0, b1, 24);
        f0_rnd(d_key, b0, b1, 28);
        f1_rnd(d_key, b0, b1, 32);
        f0_rnd(d_key, b0, b1, 36);
        f1_rnd(d_key, b0, b1, 40);
        f0_rnd(d_key, b0, b1, 44);

        gamma_tau(0, b0, b1);
        gamma_tau(1, b0, b1);
        gamma_tau(2, b0, b1);
        gamma_tau(3, b0, b1);

        in_blk[0] = (b0[0] ^ d_key[48]);
        in_blk[1] = (b0[1] ^ d_key[49]);
        in_blk[2] = (b0[2] ^ d_key[50]);
        in_blk[3] = (b0[3] ^ d_key[51]);
        System.arraycopy(in_b, 0, in_blk, 0, 4);
    }

    /**
     * Function converts long variable to array of bytes.
     * @param x long variable
     * @return array of bytes
     */
    public static byte[] longToBytes(long x) {
        ByteBuffer buffer = ByteBuffer.allocate(Long.BYTES);
        buffer.putLong(x);
        return buffer.array();
    }

    /**
     * Function converts 8 bytes to long.
     * @param bytes array of 8 bytes
     * @return long, composed of bytes from array
     */
    public static long bytesToLong(byte[] bytes) {
        ByteBuffer buffer = ByteBuffer.allocate(Long.BYTES);
        buffer.put(bytes);
        buffer.flip();//need flip
        return buffer.getLong();
    }

    /**
     * Function divides array of bytes into sub-arrays of exact size.
     * @param source    array of bytes
     * @param chunksize size of needed chunks
     * @return          byte[][] array of chunks
     */
    public static byte[][] divideArray(byte[] source, int chunksize) {
        byte[][] ret = new byte[(int)Math.ceil(source.length / (double)chunksize)][chunksize];
        int start = 0;
        for(int i = 0; i < ret.length; i++) {
            ret[i] = Arrays.copyOfRange(source, start, start + chunksize);
            start += chunksize ;
        }
        return ret;
    }

    /**
     * Service function to fill the long[4] array with 0's.
     * @param block block of longs.
     */
    public static void cleanBlock(long[] block) { for (long tmp: block) tmp = 0; }

    public static int countOnes(long number){
        int count = 0;
        long tmp;
        tmp = number;
        while(tmp != 0){
            count++;
            tmp &= (tmp-1);
        }
//        for (int i = 0; i < number.length; i++){
//            tmp = number[i];
//            while(tmp != 0){
//                count++;
//                tmp &= (tmp-1);
//            }
//        }
        return count;
    }

    public static double autoCorTest(long[] keystream, int streamLen) {
        /* d = 32 bits */
        int bitCount = streamLen * 32;
        int a = 0;
        long tmp, tmpd;
        for (int i = 0; i < streamLen - 1; i++) {
            tmp = keystream[i];
            tmpd = keystream[i+1];
            tmp ^= tmpd;
            a += countOnes(tmp);
        }
        System.out.println("A(d): " + a);
        return 2 * (a - (bitCount - 32) / 2) / Math.sqrt(bitCount - 32); // auto correlation formula
    }

    public static byte[] concatAll(byte[] first, byte[]... rest) {
        int totalLength = first.length;
        for (byte[] array : rest) {
            totalLength += array.length;
        }
        byte[] result = Arrays.copyOf(first, totalLength);
        int offset = first.length;
        for (byte[] array : rest) {
            System.arraycopy(array, 0, result, offset, array.length);
            offset += array.length;
        }
        return result;
    }

    public static double correlation(byte[] file1, byte[] file2) {
        int border = Math.min(file1.length, file2.length);
        if(file1.length == file2.length) border = file1.length;
        double sum = 0;
        for(int i = 0; i < border; i++) {
            sum += ((2*file1[i] - 1) * (2*file2[i] - 1));
        }
        sum = sum/(file1.length+file2.length);
        return sum;
    }

    public static void main(String[] args) throws IOException {
        long[] in_key = {1111, 2222, 3333, 4444, 5555, 6666, 7777, 8888};
        setup(in_key);
        Path path_in = Paths.get("/home/alex/IdeaProjects/CRYPTON/src/in_pic.png");
        Path path_enc = Paths.get("/home/alex/IdeaProjects/CRYPTON/src/out_pic.png");
        Path path_dec = Paths.get("/home/alex/IdeaProjects/CRYPTON/src/normal.png");
        byte[] file = Files.readAllBytes(path_in); // file as byte array
        // extend file-size
        int length = file.length;
        while(length % 8 != 0) length++;
        System.out.println("Bytes: " + file.length + "\nExtended: " + length);
        //
        byte[][] blocks = divideArray(file, 32); // byte array split to 8-byte chunks
        long[] block = new long[4];
        int dirtyTail = 0;
        // file output in bytes
//        for(int i = 0; i < blocks.length; i++){
//            for (int j = 0; j < blocks[i].length; j++){
//                System.out.print(blocks[i][j] + " | ");
//                if (blocks[i][j] == 0) dirtyTail++;
//            }
//            System.out.println();
//        }
        System.out.println(blocks.length);
        long[] testEnc = new long[4];
        long[] testDec = new long[4];
        System.out.println(dirtyTail);
        byte[][] tmpForBlock;
        for(int i = 0; i < blocks.length; i++){
            tmpForBlock = divideArray(blocks[i], 8);
            block[0] = bytesToLong(tmpForBlock[0]);
            block[1] = bytesToLong(tmpForBlock[1]);
            block[2] = bytesToLong(tmpForBlock[2]);
            block[3] = bytesToLong(tmpForBlock[3]);
            encrypt(block);
            Files.write(path_enc, longToBytes(block[0]));
            Files.write(path_enc, longToBytes(block[1]));
            Files.write(path_enc, longToBytes(block[2]));
            Files.write(path_enc, longToBytes(block[3]));
            decrypt(block);
            Files.write(path_dec, longToBytes(block[0]));
            Files.write(path_dec, longToBytes(block[1]));
            Files.write(path_dec, longToBytes(block[2]));
            Files.write(path_dec, longToBytes(block[3]));
        }
        //file = Files.readAllBytes(path_dec);
        byte[] fileFinal = new byte[file.length-dirtyTail];
        fileFinal = Arrays.copyOfRange(file, 0, file.length-dirtyTail);
        for(int k = 0; k < fileFinal.length; k++){
            fileFinal[k] = file[k];
//            System.out.print(fileFinal[k] + "|");
        }
        Files.write(path_dec, fileFinal);
        System.out.println("\nCorrelation: " + correlation(Files.readAllBytes(path_enc), Files.readAllBytes(path_in)));
    }
}

// https://stackoverflow.com/questions/29997665/converting-from-a-char-to-a-long
// http://javadevnotes.com/java-read-text-file-examples

// byte : 8  bit
// long : 64 bit